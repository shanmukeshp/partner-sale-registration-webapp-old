import {IdentityServiceSdkConfig} from 'identity-service-sdk';
import {SessionManagerConfig} from 'session-manager';
import {CustomerSegmentServiceSdkConfig} from 'customer-segment-service-sdk';
import {CustomerBrandServiceSdkConfig} from 'customer-brand-service-sdk';
import {AccountServiceSdkConfig} from 'account-service-sdk';
import {AccountContactServiceSdkConfig} from 'account-contact-service-sdk';
import {BuyingGroupServiceSdkConfig} from 'buying-group-service-sdk';
import {ManagementCompanyServiceSdkConfig} from 'management-company-service-sdk';
import {CustomerSourceServiceSdkConfig} from 'customer-source-service-sdk';
import {PartnerRepServiceSdkConfig} from 'partner-rep-service-sdk';
import {PartnerRepAssociationServiceSdkConfig} from 'partner-rep-association-service-sdk';
import {PartnerSaleRegistrationDraftServiceSdkConfig} from 'partner-sale-registration-draft-service-sdk';
import {ProductGroupServiceSdkConfig} from 'product-group-service-sdk';
import {ProductLineServiceSdkConfig} from 'product-line-service-sdk';
import {AssetServiceSdkConfig} from 'asset-service-sdk';
import {PartnerSaleInvoiceServiceSdkConfig} from 'partner-sale-invoice-service-sdk';


export default class Config {

    _identityServiceSdkConfig:IdentityServiceSdkConfig;

    _customerSegmentServiceSdkConfig:CustomerSegmentServiceSdkConfig;

    _customerBrandServiceSdkConfig:CustomerBrandServiceSdkConfig;

    _accountServiceSdkConfig:AccountServiceSdkConfig;

    _accountContactServiceSdkConfig:AccountContactServiceSdkConfig;

    _buyingGroupServiceSdkConfig:BuyingGroupServiceSdkConfig;

    _managementCompanyServiceSdkConfig:ManagementCompanyServiceSdkConfig;

    _customerSourceServiceSdkConfig:CustomerSourceServiceSdkConfig;

    _partnerRepServiceSdkConfig:PartnerRepServiceSdkConfig;

    _partnerRepAssociationServiceSdkConfig:PartnerRepAssociationServiceSdkConfig;

    _sessionManagerConfig:SessionManagerConfig;

    _partnerSaleRegistrationDraftServiceSdkConfig:PartnerSaleRegistrationDraftServiceSdkConfig;

    _productGroupServiceSdkConfig:ProductGroupServiceSdkConfig;

    _productLineServiceSdkConfig:ProductLineServiceSdkConfig;

    _assetServiceSdkConfig:AssetServiceSdkConfig;

    _partnerSaleInvoiceServiceSdkConfig:PartnerSaleInvoiceServiceSdkConfig;

    /**
     *
     * @param identityServiceSdkConfig
     * @param customerSegmentServiceSdkConfig
     * @param customerBrandServiceSdkConfig
     * @param accountServiceSdkConfig
     * @param accountContactServiceSdkConfig
     * @param buyingGroupServiceSdkConfig
     * @param managementCompanyServiceSdkConfig
     * @param customerSourceServiceSdkConfig
     * @param partnerRepServiceSdkConfig
     * @param partnerRepAssociationServiceSdkConfig
     * @param sessionManagerConfig
     * @param partnerSaleRegistrationDraftServiceSdkConfig
     * @param productGroupServiceSdkConfig
     * @param productLineServiceSdkConfig
     * @param assetServiceSdkConfig
     * @param partnerSaleInvoiceServiceSdkConfig
     */
    constructor(identityServiceSdkConfig:IdentityServiceSdkConfig,
                customerSegmentServiceSdkConfig:CustomerSegmentServiceSdkConfig,
                customerBrandServiceSdkConfig:CustomerBrandServiceSdkConfig,
                accountServiceSdkConfig:AccountServiceSdkConfig,
                accountContactServiceSdkConfig:AccountContactServiceSdkConfig,
                buyingGroupServiceSdkConfig:BuyingGroupServiceSdkConfig,
                managementCompanyServiceSdkConfig:ManagementCompanyServiceSdkConfig,
                customerSourceServiceSdkConfig:CustomerSourceServiceSdkConfig,
                partnerRepServiceSdkConfig:PartnerRepServiceSdkConfig,
                partnerRepAssociationServiceSdkConfig:PartnerRepAssociationServiceSdkConfig,
                sessionManagerConfig:SessionManagerConfig,
                partnerSaleRegistrationDraftServiceSdkConfig:PartnerSaleRegistrationDraftServiceSdkConfig,
                productGroupServiceSdkConfig:ProductGroupServiceSdkConfig,
                productLineServiceSdkConfig:ProductLineServiceSdkConfig,
                assetServiceSdkConfig:AssetServiceSdkConfig,
                partnerSaleInvoiceServiceSdkConfig:PartnerSaleInvoiceServiceSdkConfig
    ) {

        if (!identityServiceSdkConfig) {
            throw new TypeError('identityServiceSdkConfig required');
        }
        this._identityServiceSdkConfig = identityServiceSdkConfig;


        if (!customerSegmentServiceSdkConfig) {
            throw new TypeError('customerSegmentServiceSdkConfig required');
        }

        this._customerSegmentServiceSdkConfig = customerSegmentServiceSdkConfig;

        if (!customerBrandServiceSdkConfig) {
            throw new TypeError('customerBrandServiceSdkConfig required');
        }

        this._customerBrandServiceSdkConfig = customerBrandServiceSdkConfig;

        if (!accountServiceSdkConfig) {
            throw new TypeError('accountServiceSdkConfig required');
        }

        this._accountServiceSdkConfig = accountServiceSdkConfig;

        if (!accountContactServiceSdkConfig) {
            throw new TypeError('accountContactServiceSdkConfig required');
        }

        this._accountContactServiceSdkConfig = accountContactServiceSdkConfig;

        if (!buyingGroupServiceSdkConfig) {
            throw new TypeError('buyingGroupServiceSdkConfig required');
        }

        this._buyingGroupServiceSdkConfig = buyingGroupServiceSdkConfig;


        if (!managementCompanyServiceSdkConfig) {
            throw new TypeError('managementCompanyServiceSdkConfig required');
        }

        this._managementCompanyServiceSdkConfig = managementCompanyServiceSdkConfig;


        if (!customerSourceServiceSdkConfig) {
            throw new TypeError('customerSourceServiceSdkConfig required');
        }

        this._customerSourceServiceSdkConfig = customerSourceServiceSdkConfig;

        if (!partnerRepAssociationServiceSdkConfig) {
            throw new TypeError('partnerRepAssociationServiceSdkConfig required');
        }

        this._partnerRepAssociationServiceSdkConfig = partnerRepAssociationServiceSdkConfig;

        if (!partnerRepServiceSdkConfig) {
            throw new TypeError('partnerRepServiceSdkConfig required');
        }

        this._partnerRepServiceSdkConfig = partnerRepServiceSdkConfig;

        if (!sessionManagerConfig) {
            throw new TypeError('sessionManagerConfig required');
        }
        this._sessionManagerConfig = sessionManagerConfig;

        if (!partnerSaleRegistrationDraftServiceSdkConfig) {
            throw new TypeError('partnerSaleRegistrationDraftServiceSdkConfig required');
        }
        this._partnerSaleRegistrationDraftServiceSdkConfig = partnerSaleRegistrationDraftServiceSdkConfig;

        if (!productGroupServiceSdkConfig) {
            throw new TypeError('productGroupServiceSdkConfig required');
        }
        this._productGroupServiceSdkConfig = productGroupServiceSdkConfig;

        if (!productLineServiceSdkConfig) {
            throw new TypeError('productLineServiceSdkConfig required');
        }
        this._productLineServiceSdkConfig = productLineServiceSdkConfig;

        if (!assetServiceSdkConfig) {
            throw new TypeError('assetServiceSdkConfig required');
        }
        this._assetServiceSdkConfig = assetServiceSdkConfig;
        if (!partnerSaleInvoiceServiceSdkConfig) {
            throw new TypeError('partnerSaleInvoiceServiceSdkConfig required');
        }
        this._partnerSaleInvoiceServiceSdkConfig = partnerSaleInvoiceServiceSdkConfig;


    }

    get identityServiceSdkConfig() {
        return this._identityServiceSdkConfig;
    }


    get customerSegmentServiceSdkConfig() {
        return this._customerSegmentServiceSdkConfig;
    }

    get customerBrandServiceSdkConfig() {
        return this._customerBrandServiceSdkConfig;
    }

    get accountContactServiceSdkConfig() {
        return this._accountContactServiceSdkConfig;
    }

    get accountServiceSdkConfig() {
        return this._accountServiceSdkConfig;
    }

    get buyingGroupServiceSdkConfig() {
        return this._buyingGroupServiceSdkConfig;
    }

    get managementCompanyServiceSdkConfig() {
        return this._managementCompanyServiceSdkConfig;
    }

    get customerSourceServiceSdkConfig() {
        return this._customerSourceServiceSdkConfig;
    }

    get partnerRepAssociationServiceSdkConfig() {
        return this._partnerRepAssociationServiceSdkConfig;
    }

    get partnerRepServiceSdkConfig() {
        return this._partnerRepServiceSdkConfig;
    }

    get sessionManagerConfig() {
        return this._sessionManagerConfig;
    }

    get partnerSaleRegistrationDraftServiceSdkConfig() {
        return this._partnerSaleRegistrationDraftServiceSdkConfig;
    }

    get productGroupServiceSdkConfig(){
        return this._productGroupServiceSdkConfig;
    }

    get productLineServiceSdkConfig(){
        return this._productLineServiceSdkConfig;
    }

    get assetServiceSdkConfig(){
        return this._assetServiceSdkConfig;
    }

    get partnerSaleInvoiceServiceSdkConfig() {
        return this._partnerSaleInvoiceServiceSdkConfig;
    }
}