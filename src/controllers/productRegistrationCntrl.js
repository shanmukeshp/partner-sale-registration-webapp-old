import IdentityServiceSdk from 'identity-service-sdk';
import SessionManager from 'session-manager';
import {AddCommercialAccountReq} from 'account-service-sdk';
import {AddAccountContactReq} from 'account-contact-service-sdk';
import PartnerSaleRegistrationServiceSdk from 'partner-sale-registration-draft-service-sdk';
import ProductGroupServiceSdk from 'product-group-service-sdk';
import ProductLineServiceSdk from 'product-line-service-sdk';
import AssetServiceSdk from 'asset-service-sdk';
import PartnerSaleInvoiceServiceSdk from 'partner-sale-invoice-service-sdk';
import PartnerRepServiceSdk from 'partner-rep-service-sdk';

angular
    .module('productRegistrationCntrl',['ui.bootstrap']);

export default class productRegistrationCntrl {
    constructor($scope,$rootScope, $q,Upload,config, $uibModal,$location, sessionManager, identityServiceSdk, accountServiceSdk, accountContactServiceSdk,
                buyingGroupServiceSdk, managementCompanyServiceSdk, customerSourceServiceSdk, partnerRepServiceSdk, partnerRepAssociationServiceSdk,partnerSaleRegistrationServiceSdk,
                productLineServiceSdk,productGroupServiceSdk,partnerSaleInvoiceServiceSdk,assetServiceSdk,addPartnerRepModal) {


        //populate facilities drop down
        $rootScope.finalResponse=function(){
            $scope.isEdit=false;
            $scope.loader=false;
            $scope.productRegistration={};
            $scope.addNewFacility={};
            $scope.facilityInfo={};
            $scope.isDisabled=false;
            $scope.ProductRegistrationPageLoad();
            $rootScope.facilityRecords();
        };
        $rootScope.facilityRecords=function(){
            $q(
                resolve =>
                    sessionManager
                        .getAccessToken()
                        .then(
                            accessToken =>
                                resolve(accessToken)
                        )
            ).then(
                accessToken => {
                    $scope.loader=true;

                    $q(
                        resolve =>
                            identityServiceSdk
                                .getUserInfo(accessToken)
                                .then(
                                    userInfo =>
                                        resolve(userInfo)
                                )
                    ).then(userInfo => {
                        $scope.loader=true;
                        $scope.accountId=userInfo._account_id;
                        $q(
                            resolve =>
                                accountServiceSdk
                                    .searchCommercialAccountsAssociatedWithPartnerAccountId(userInfo._account_id, accessToken)
                                    .then(
                                        response =>
                                            resolve(response)
                                    )
                        ).then(facilities => {
                            $scope.loader=false;
                            console.log("facilities:", facilities);
                            $scope.facilities = facilities;
                        });
                    });
                });
        };
        $scope.ProductRegistrationPageLoad=function(){

            $scope.facilityRecords();
            $rootScope.ok_addFacility=function(){
                $scope.submitted=false;
                $scope.modalInstance.dismiss('cancel');
                $scope.addNewFacility={};
                $scope.finalResponse();
            };
            //populate buying group dropdown
            $q(
                resolve =>
                    sessionManager
                        .getAccessToken()
                        .then(
                            accessToken =>
                                resolve(accessToken)
                        )
            ).then(
                accessToken => {
                    $scope.loader=true;
                    $q(
                        resolve =>
                            buyingGroupServiceSdk
                                .listBuyingGroups(accessToken)
                                .then(
                                    response =>
                                        resolve(response)
                                )
                    ).then(buyingGroups => {
                        $scope.loader=false;
                        console.log("buyingGroups:", buyingGroups.response);
                        $scope.buyingGroups = JSON.parse(buyingGroups.response);
                    });
                });

            //populate management companies dropdown

            $q(
                resolve =>
                    sessionManager
                        .getAccessToken()
                        .then(
                            accessToken =>
                                resolve(accessToken)
                        )
            ).then(
                accessToken => {
                    $scope.loader=true;
                    $q(resolve =>
                        managementCompanyServiceSdk
                            .listManagementCompanies(accessToken)
                            .then(
                                response =>
                                    resolve(response)
                            )
                    ).then(managementCompanies => {
                        $scope.loader=false;
                        console.log("managementCompanies:", managementCompanies.response);
                        $scope.managementCompanies = JSON.parse(managementCompanies.response);
                    });
                });

            //populate customer source dropdown

            $q(
                resolve =>
                    sessionManager
                        .getAccessToken()
                        .then(
                            accessToken =>
                                resolve(accessToken)
                        )
            ).then(
                accessToken => {
                    $scope.loader=true;
                    $q(
                        resolve =>
                            customerSourceServiceSdk
                                .listCustomerSources(accessToken)
                                .then(
                                    response =>
                                        resolve(response)
                                )
                    ).then(customerSources => {
                        $scope.loader=false;
                        console.log("customerSources:", customerSources);
                        $scope.customerSources = customerSources;
                    });
                });


            //populate dealer rep dropdown

            $q(
                resolve =>
                    sessionManager
                        .getAccessToken()
                        .then(
                            accessToken =>
                                resolve(accessToken)
                        )
            ).then(
                accessToken => {
                    $scope.loader=true;
                    $q(
                        resolve =>
                            identityServiceSdk
                                .getUserInfo(accessToken)
                                .then(
                                    userInfo =>
                                        resolve(userInfo)
                                )
                    ).then(userInfo => {
                        $q(
                            resolve =>
                                partnerRepAssociationServiceSdk
                                    .listPartnerRepAssociationsWithPartnerId(userInfo._account_id, accessToken)
                                    .then(partnerRepAssociations =>
                                        resolve(partnerRepAssociations)
                                    )
                        ).then(partnerRepAssociations => {
                            $scope.loader=false;
                            console.log("partnerRepAssociations:", partnerRepAssociations);
                            var partnerRepIds = Array.from(partnerRepAssociations, (partnerRepAssociation) => {
                                    return partnerRepAssociation.repId;
                                }
                            );

                            console.log("partnerRepIds:", partnerRepIds);

                            $q(
                                resolve =>
                                    partnerRepServiceSdk
                                        .getPartnerRepsWithIds(partnerRepIds, accessToken)
                                        .then(partnerReps =>
                                            resolve(partnerReps)
                                        )
                            ).then(partnerReps => {
                                $scope.loader=false;
                                console.log("partnerReps:", partnerReps);
                                $scope.dealers = partnerReps;
                            });
                        });
                    });
                });

            $scope.onFacilitySelected = function() {
                $scope.loader=false;
                // console.log("id is",id);
                console.log("selected facility:", $scope.productRegistration.selectedFacility.id);
                if($scope.productRegistration.selectedFacility==null)
                {
                    $scope.isDisabled=false;
                }
                else{
                    $q(
                        resolve =>
                            sessionManager
                                .getAccessToken()
                                .then(
                                    accessToken =>
                                        resolve(accessToken)
                                )
                    ).then(
                        accessToken => {
                            $scope.loader=true;
                            $q(
                                resolve =>
                                    identityServiceSdk
                                        .getUserInfo(accessToken)
                                        .then(
                                            userInfo =>
                                                resolve(userInfo)
                                        )
                            ).then(userInfo => {
                                $q(
                                    resolve =>
                                        accountServiceSdk
                                            .getCommercialAccountWithId($scope.productRegistration.selectedFacility.id, accessToken)
                                            .then(
                                                response =>
                                                    resolve(response)
                                            )
                                ).then(facilityInfo => {
                                    $scope.loader=false;
                                    console.log("facilityInfo:", JSON.parse(JSON.stringify(facilityInfo)));
                                    $scope.facilityInfo = JSON.parse(JSON.stringify(facilityInfo));
                                });
                                $q(
                                    resolve =>
                                        accountContactServiceSdk
                                            .listAccountContactsWithAccountId($scope.productRegistration.selectedFacility.id, accessToken)
                                            .then(
                                                response =>
                                                    resolve(response)
                                            )
                                ).then(facilityContacts => {
                                    $scope.loader=false;
                                    console.log("facilityContacts:", JSON.parse(JSON.stringify(facilityContacts)));
                                    $scope.facilityContacts = JSON.parse(JSON.stringify(facilityContacts));
                                });
                            });
                        });
                    $scope.isDisabled=true;
                }
            };
            //AddNewContactInfo Begin
            //AddNewContcatInfo End
            $scope.addNewContact = function(isValid) {
                $scope.loader=false;
                $scope.submitted=true;
                if(isValid){
                    $scope.loader=true;
                    var addAccountContactReq = new AddAccountContactReq($scope.productRegistration.selectedFacility.id,
                        $scope.addNewContact.firstName,
                        $scope.addNewContact.lastName,
                        $scope.addNewContact.contactPhone,
                        $scope.addNewContact.contactEmail,
                        $scope.productRegistration.selectedFacility.address.countryIso31661Alpha2Code,
                        $scope.productRegistration.selectedFacility.address.regionIso31662Code
                    );
                    $q(
                        resolve =>
                            sessionManager
                                .getAccessToken()
                                .then(
                                    accessToken =>
                                        resolve(accessToken)
                                )
                    ).then(
                        accessToken => {
                            $scope.loader=true;
                            $q(
                                resolve =>
                                    accountContactServiceSdk
                                        .addAccountContact(addAccountContactReq, accessToken)
                                        .then(
                                            response =>
                                                resolve(response)
                                        )
                            ).then(response => {
                                $scope.loader=false;
                                console.log("add account contact response :", response);
                                $scope.modalInstance.close();
                                $scope.modalInstance = $uibModal.open({
                                    scope:$scope,
                                    template: '<div class="modal-header"> <h4 class="modal-title">Success !</h4></div>' +
                                    '<div class="modal-body">ContactInfo Added Successfully</div>' +
                                    '<div class="modal-footer">' +
                                    '<button class="btn btn-primary" type="button" ng-click="ok_addContact()">ok</button></div>',
                                    size:'sm',
                                    backdrop : 'static'
                                });
                            });
                        }
                    );
                }
            };
            $scope.ok_addContact=function(){
                $scope.modalInstance.close();
                $scope.onFacilitySelected();
            }
            //Start date picker
            $scope.today = function() {
                $scope.sellDate = new Date();
                $scope.installDate = new Date();
            };
            $scope.today();

            $scope.clear = function () {
                $scope.sellDate = null;
                $scope.installDate = null;
            };

            // Disable weekend selection


            $scope.open = function($event) {
                $scope.status.opened = true;
            };
            $scope.open1 = function($event) {
                $scope.status.opened1 = true;
            };

            $scope.setDate = function(year, month, day) {
                $scope.sellDate = new Date(year, month, day);
                $scope.installDate = new Date(year, month, day);
            };

            $scope.dateOptions = {
                formatYear: 'yy',
                startingDay: 1
            };

            $scope.formats = ['dd-MMMM-yyyy', 'MM/dd/yyyy', 'dd.MM.yyyy', 'shortDate'];
            $scope.format = $scope.formats[1];

            $scope.status = {
                opened: false
            };

            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            var afterTomorrow = new Date();
            afterTomorrow.setDate(tomorrow.getDate() + 2);
            $scope.events =
                [
                    {
                        date: tomorrow,
                        status: 'full'
                    },
                    {
                        date: afterTomorrow,
                        status: 'partially'
                    }
                ];

            $scope.getDayClass = function(date, mode) {
                if (mode === 'day') {
                    var dayToCheck = new Date(date).setHours(0,0,0,0);

                    for (var i=0;i<$scope.events.length;i++){
                        var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

                        if (dayToCheck === currentDay) {
                            return $scope.events[i].status;
                        }
                    }
                }

                return '';
            };

            //End datepicker
            //response for facility dropdown
            ///
            $scope.addFacility = function (size) {
                $scope.submitted=false;
                $scope.modalInstance = $uibModal.open({
                    scope:$scope,
                    templateUrl: 'templates/popupModals/addFacility.html',
                    size: size,
                    backdrop : 'static'

                });
            };
            $scope.newContact= function (size) {
                $scope.submitted=false;
                $scope.modalInstance = $uibModal.open({
                    scope:$scope,
                    templateUrl: 'templates/popupModals/addNewContact.html',
                    size: size,
                    backdrop : 'static'

                });
            };
            $scope.addProduct=function(size){
                $scope.modalInstance = $uibModal.open({
                    scope:$scope,
                    templateUrl: 'templates/popupModals/AddProduct.html',
                    size: size,
                    backdrop : 'static'

                });
                $scope.loader=false;
                $scope.prodJson=[];
                //populate product group dropdown
                $q(
                    resolve =>
                        sessionManager
                            .getAccessToken()
                            .then(
                                accessToken =>
                                    resolve(accessToken)
                            )
                ).then(
                    accessToken => {
                        $scope.loader=true;
                        $q(
                            resolve =>
                                productGroupServiceSdk
                                    .listProductGroups(accessToken)
                                    .then(
                                        response =>
                                            resolve(response)
                                    )
                        ).then(listProductGroups => {
                            $scope.loader=false;
                            console.log("ProductGroups :", listProductGroups);
                            $scope.productsTypeList = listProductGroups;
                        });
                    });
                //populate productLinesList companies dropdown
                 $q(
                     resolve =>
                         sessionManager
                             .getAccessToken()
                             .then(
                                 accessToken =>
                                     resolve(accessToken)
                             )
                 ).then(
                     accessToken => {
                         $scope.loader=true;
                         $q(
                             resolve =>
                                 identityServiceSdk
                                     .getUserInfo(accessToken)
                                     .then(
                                         userInfo =>
                                             resolve(userInfo)
                                     )
                         ).then(userInfo => {
                             $scope.loader=true;
                             $q(
                                 resolve =>
                                     assetServiceSdk
                                         .listAssetsWithAccountId($scope.accountId, accessToken)
                                         .then(
                                             response =>
                                                 resolve(response)
                                         )
                             ).then(AssetSynopsisView => {
                                 $scope.loader=false;
                                 $scope.AddProdctGridList = JSON.parse(JSON.stringify(AssetSynopsisView));
                             });
                         });
                     });
            };

            $scope.Back_Drafts=function(){
                $location.path("/");
            };

            $scope.setColor=function(len){
                if(len>1){
                    return "bgColor";
                }
            }

            $scope.prodJson=[];
            var testObj ={};
            var i=0;
            /*$scope.prodListJson={};*/
            var selectedRecord;
            $scope.Selected_ProductLine=function(record){
                selectedRecord = record;
                $scope.idSelectedVote = record.serialNumber;
            }
            $scope.addComponent = function () {

                if(Object.keys(selectedRecord).length>0 && $scope.prodJson.indexOf(selectedRecord)==-1){
                    $scope.prodJson.push(selectedRecord);
                }
            };
            $scope.saveandClose = function () {
                if(Object.keys($scope.prodJson).length>0){
                    var values =[];
                    var tempValues =[];
                    if(Object.keys(testObj).length>0){
                        angular.forEach(Object.values(testObj),function(value){
                            values = values.concat(value);
                        });
                        angular.forEach($scope.prodJson,function(value){
                            if(!isExits(values,value)){
                                tempValues.push(value);
                            }
                        });
                    }else{
                        testObj[i] = $scope.prodJson;
                        i++;
                    }
                    if(tempValues.length>0){
                        testObj[i] = tempValues;
                        i++;
                    }
                    $scope.prodListJson=testObj;

                }
                console.log("listjson",$scope.prodListJson);
                $scope.modalInstance.close();
                $("#error_product").hide();
            };

            $scope.saveandAnother = function (size) {
                if(Object.keys($scope.prodJson).length>0){
                    var values =[];
                    var tempValues =[];
                    if(Object.keys(testObj).length>0){
                        angular.forEach(Object.values(testObj),function(value){
                            values = values.concat(value);
                        });

                        angular.forEach($scope.prodJson,function(value){
                            if(!isExits(values,value)){
                                tempValues.push(value);

                            }
                        });
                    }else{
                        testObj[i] = $scope.prodJson;
                        i++;
                    }
                    if(tempValues.length>0){
                        testObj[i] = tempValues;
                        i++;
                    }
                    $scope.prodListJson=testObj;

                    $scope.prodJson=[];
                }

                $scope.modalInstance.close();
                $scope.modalInstance = $uibModal.open({
                    scope:$scope,
                    templateUrl: 'templates/popupModals/AddProduct.html',
                    size: size,
                    backdrop : 'static'

                });
                $("#error_product").hide();
            };
            function isExits(arr,val){
                var isValueExits = false;
                angular.forEach(arr,function(value){
                    if(val.serialNumber == value.serialNumber){
                        isValueExits =  true;
                    }
                });
                return isValueExits;
            }
            $scope.AddProductnotListed = function (size) {
                $scope.addNewProductnotListed={};
              //  $scope.submitted=false;
                $scope.modalInstance.close();
                $scope.modalInstance = $uibModal.open({
                    scope:$scope,
                    templateUrl: 'templates/popupModals/addProductnotListed.html',
                    size: size,
                    backdrop : 'static'

                });
               // $scope.productLines=$scope.productsTypeList;
                $q(
                    resolve =>
                        sessionManager
                            .getAccessToken()
                            .then(
                                accessToken =>
                                    resolve(accessToken)
                            )
                ).then(
                    accessToken => {
                        $scope.loader=true;
                        $q(
                            resolve =>
                                identityServiceSdk
                                    .getUserInfo(accessToken)
                                    .then(
                                        userInfo =>
                                            resolve(userInfo)
                                    )
                        ).then(userInfo => {
                            $scope.loader=true;
                            $q(
                                resolve =>
                                    productLineServiceSdk
                                        .listProductLines(accessToken)
                                        .then(
                                            response =>
                                                resolve(response)
                                        )
                            ).then(productLineIds => {
                                $scope.loader=false;
                                console.log("productLines:", productLineIds);
                                $scope.productLines = productLineIds;
                                $scope.lineIDs=[];
                                angular.forEach($scope.productLineIDs,function(v,k){
                                    $scope.lineIDs.push(v.id);
                                });

                            });

                        });
                    });


                //$scope.modalInstance.close();
            };
            $scope.addNewProductnotListed={};
$scope.addNewProductLine=function(){
    var req={};
    req.accountId=$scope.accountId;
    req.productLineId= $scope.addNewProductnotListed.productLine.id;
    req.serialNumber=$scope.addNewProductnotListed.productSerialNumber;
    req.description=$scope.addNewProductnotListed.productDescription;

    $q(
        resolve =>
            sessionManager
                .getAccessToken()
                .then(
                    accessToken =>
                        resolve(accessToken)
                )
    ).then(
        accessToken => {
            $scope.loader=true;
            $q(
                resolve =>
                    identityServiceSdk
                        .getUserInfo(accessToken)
                        .then(
                            userInfo =>
                                resolve(userInfo)
                        )
            ).then(userInfo => {
                $scope.loader=true;
                $q(
                    resolve =>
                        assetServiceSdk
                            .addAsset(req, accessToken)
                            .then(
                                response =>
                                    resolve(response)
                            )
                ).then(AssestResponse => {
                    $scope.loader=false;
                    $scope.modalInstance.close();
                    console.log("productAddAseetresponse:", AssestResponse);
                    //$scope.AddProdctGridList = AssetSynopsisView;
                });

            });
        });

};
            $scope.ok = function () {
                $scope.modalInstance.close();
            };

            $scope.cancel = function () {
                $scope.modalInstance.dismiss('cancel');
            };
            $scope.compositeLineItems=[];
            $scope.simpleLineItems=[];
//next submit
            $scope.handleFileSelection = function($files) {
                $scope.invoiceFileToUpload = $files[0];
            }
            $scope.save_next_submit=function(){
                var error=false;
                if(!$scope.productRegistration.selectedFacility){ $scope.productRegistrationForm.selectFacility.$dirty = true; $scope.productRegistrationForm.selectFacility.$invalid = true ; error = true;}
                if(!$scope.productRegistration.contactInfo){ $scope.productRegistrationForm.contactInfo.$dirty = true; $scope.productRegistrationForm.contactInfo.$invalid = true ; error = true;}
                if(!$scope.productRegistration.sellDate){ $scope.productRegistrationForm.sellDate.$dirty = true; $scope.productRegistrationForm.sellDate.$invalid = true ; error = true;}
                if(!$scope.productRegistration.installDate){ $scope.productRegistrationForm.installDate.$dirty = true; $scope.productRegistrationForm.installDate.$invalid = true ; error = true;}
                if(!$scope.productRegistration.invoiceNumber){ $scope.productRegistrationForm.invoiceNumber.$dirty = true; $scope.productRegistrationForm.invoiceNumber.$invalid = true ; error = true;}
                if(!$scope.prodListJson){$("#error_product").show();error=true;}
                if (!error){
                    $scope.loader=true;
                    $q(
                        resolve =>
                            sessionManager
                                .getAccessToken()
                                .then(
                                    accessToken =>
                                        resolve(accessToken)
                                )
                    ).then(
                        accessToken => {
                            var partnerSaleInvoiceNumber = $scope.productRegistration.invoiceNumber;
                            var baseUrl = config.partnerSaleInvoiceServiceSdkConfig.precorConnectApiBaseUrl;
                            console.log("invoicenumber",partnerSaleInvoiceNumber);

                            if($scope.invoiceFileToUpload) {
                                /**
                                 * TODO: Try to move this to javascript SDK.
                                 */
                                Upload.upload({
                                    url: baseUrl + '/partner-sale-invoices',
                                    headers: {
                                        'Authorization': `Bearer ${accessToken}`
                                    },
                                    data: {number:partnerSaleInvoiceNumber,file: $scope.invoiceFileToUpload}
                                }).then(response => {
                                        console.log("upload succeeded:", response.data);
                                        $scope.invoiceId=response.data;
                                        $q(
                                            resolve =>
                                                partnerSaleInvoiceServiceSdk
                                                    .getPartnerSaleInvoiceWithId($scope.invoiceId, accessToken)
                                                    .then(
                                                        PartnerSaleInvoiceView =>
                                                            resolve(PartnerSaleInvoiceView)
                                                    )
                                        ).then(PartnerSaleInvoiceView => {
                                            console.log("invoiceURL Response:", PartnerSaleInvoiceView._fileUrl);
                                            $scope.productRegistration.invoiceUrl=PartnerSaleInvoiceView._fileUrl;
                                            $scope.draftRequest();
                                        });

                                    },function (error) {
                                        console.log("Invoice upload error :", error);
                                    }
                                );

                            }
                            else{
                                console.log("file is undefined");
                                $scope.productRegistration.invoiceUrl=null;
                                $scope.draftRequest();
                            }
                        }
                    );

                }
                $scope.draftRequest=function(){
                    angular.forEach($scope.prodListJson,function(v,k){
                        console.log("values",v);
                        if(v.length>1){
                            var json={};
                            json["components"] = v;
                            $scope.compositeLineItems.push(json);
                        }
                        else{
                            $scope.simpleLineItems.push(v[0]);
                        }
                    });

                    var request={};
                    var buyingGroupId = ($scope.productRegistration.buyingGroup==undefined) ? null: $scope.productRegistration.buyingGroup;
                    console.log('buyingGroupId',buyingGroupId);
                    request.partnerAccountId=$scope.accountId;
                    request.facilityName=$scope.productRegistration.selectedFacility.name;
                    request.customerBrandName=$scope.facilityInfo.customerBrand;
                    request.facilityId=$scope.productRegistration.selectedFacility.id;
                    request.buyingGroupId=buyingGroupId;
                    ($scope.productRegistration.managementCompany==undefined)? request.managementCompanyId=null:request.managementCompanyId=$scope.productRegistration.managementCompany;
                    ($scope.productRegistration.customerSource==undefined)? request.customerSourceId=null:request.customerSourceId=$scope.productRegistration.customerSource;
                    request.installDate=$scope.productRegistration.installDate;
                    request.sellDate=$scope.productRegistration.sellDate;
                    request.invoiceNumber=$scope.productRegistration.invoiceNumber;
                    request.facilityContactId=$scope.productRegistration.contactInfo;
                    ($scope.productRegistration.dealerRep==undefined)? request.partnerRepUserId=null:request.partnerRepUserId=$scope.productRegistration.dealerRep;
                    request.simpleLineItems=$scope.simpleLineItems;
                    request.compositeLineItems=$scope.compositeLineItems;
                    request.invoiceUrl= $scope.productRegistration.invoiceUrl;
                    request.isSubmitted=false;
                    console.log("total",request);
                    $q(
                        resolve =>
                            sessionManager
                                .getAccessToken()
                                .then(
                                    accessToken =>
                                        resolve(accessToken)
                                )
                    ).then(
                        accessToken => {
                            $q(
                                resolve =>
                                    partnerSaleRegistrationServiceSdk
                                        .addPartnerCommercialSaleRegDraft(request, accessToken)
                                        .then(
                                            response =>
                                                resolve(response)
                                        )
                            ).then(returnedDraftId => {
                                console.log('returnedDraftId',returnedDraftId);
                                window.localStorage.setItem("draftId",returnedDraftId);
                                $location.path('/productRegistrationPricing');
                                /*   $scope.modalInstance = $uibModal.open({
                                 scope:$scope,
                                 template: '<div class="modal-header"> <h3 class="modal-title">Success</h3></div>' +
                                 '<div class="modal-body">Records are saved successfully</div>' +
                                 '<div class="modal-footer">' +
                                 '<button class="btn btn-primary" type="button" ng-click="Next_success()">Ok</button>'+
                                 '<button class="btn btn-warning" type="button" ng-click="cancel()">Cancel</button></div>'
                                 });*/

                            });
                        });

                    $scope.productRegistrationForm.$setPristine();
                    // $scope.productRegistration={};
                    $scope.isDisabled=false;
                    $scope.compositeLineItems=[];
                    $scope.simpleLineItems=[];
                };
            };
            /* $scope.Next_success=function(){
             $location.path('/productRegistrationPricing');
             }*/
            $scope.saveNext_cancel=function(){
                $scope.modalInstance = $uibModal.open({
                    scope:$scope,
                    template: '<div class="modal-header"> ' +
                    '<h4 class="modal-title">Warning!</h4></div>' +
                    '<div class="modal-body">Do you want to cancel Registration ?</div>' +
                    '<div class="modal-footer">' +
                    '<button class="btn btn-primary" type="button" ng-click="cancel_confirm()">Yes</button>'+
                    '<button class="btn btn-warning" type="button" ng-click="cancel_close()">No</button></div>',
                    size:'sm',
                    backdrop : 'static'
                });
            };
            $scope.cancel_confirm=function(){
                $scope.modalInstance.close();
                $scope.isDisabled=false;
                $scope.productRegistrationForm.$setPristine();
                $scope.productRegistration={};
                $scope.prodListJson={};
                $scope.facilityInfo={};
                $("#uploadFile").val('');
            };
            $scope.cancel_close=function(){
                $scope.modalInstance.dismiss('cancel');
            };

            $scope.remove_line_item=function(i){
                $scope.prodJson.splice(i,1);
            }
            $scope.showAddPartnerRepModal=function() {

                addPartnerRepModal.show();

            }

        }

    }
}
productRegistrationCntrl.$inject=[
    '$scope',
    '$rootScope',
    '$q',
    'Upload',
    'config',
    '$uibModal',
    '$location',
    'sessionManager',
    'identityServiceSdk',
    'accountServiceSdk',
    'accountContactServiceSdk',
    'buyingGroupServiceSdk',
    'managementCompanyServiceSdk',
    'customerSourceServiceSdk',
    'partnerRepServiceSdk',
    'partnerRepAssociationServiceSdk',
    'partnerSaleRegistrationServiceSdk',
    'productLineServiceSdk',
    'productGroupServiceSdk',
    'partnerSaleInvoiceServiceSdk',
    'assetServiceSdk',
    'addPartnerRepModal'
];