import IdentityServiceSdk from 'identity-service-sdk';
import SessionManager from 'session-manager';

angular
    .module('productRegistrationSubmitCntrl',['ui.bootstrap']);
export default class productRegistrationReviewCntrl{
    constructor($scope, $uibModal,$location) {
        var submitDraft=JSON.parse(window.localStorage.getItem("SubmittedDraft"));
        console.log("submitDraft",submitDraft);
        $scope.productsSubmitList=submitDraft;

        $scope.productRegistrationHome=function(){
            $location.path("/productRegistration");
        };
        $scope.Back_Drafts=function(){
            $location.path("/");
        };

        function convertUTCDateToLocalDate(date) {
            var newDate = new Date(date.getTime()+date.getTimezoneOffset()*60*1000);
            var offset = date.getTimezoneOffset() / 60;
            var hours = date.getHours();
            newDate.setHours(hours - offset);
            return newDate;
        }
        var date = convertUTCDateToLocalDate(new Date($scope.productsSubmitList.draftSubmittedDate));
        $scope.productsSubmitList.draftSubmittedDate=date.toLocaleString();

    }
};

productRegistrationReviewCntrl.$inject=[
    '$scope',
    '$uibModal',
    '$location'
];